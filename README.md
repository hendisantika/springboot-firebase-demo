# Spring Boot Firebase Demo

### Things to do list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-firebase-demo.git`
2. Go inside the folder: `cd springboot-firebase-demo`
3. Run the application: `mvn clean spring-boot:run`