package com.hendisantika.springbootfirebasedemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-firebase-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/04/21
 * Time: 09.22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private String name;
    private String description;
}
